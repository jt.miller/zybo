# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\projects\vsmd\zybo\manual\vitis\zybo_freertos\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\projects\vsmd\zybo\manual\vitis\zybo_freertos\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {zybo_freertos}\
-hw {C:\projects\vsmd\zybo\manual\vitis\zybo.xsa}\
-proc {ps7_cortexa9_0} -os {freertos10_xilinx} -out {C:/projects/vsmd/zybo/manual/vitis}

platform write
platform generate -domains 
platform active {zybo_freertos}
platform generate
